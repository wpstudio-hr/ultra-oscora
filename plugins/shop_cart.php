<?php

class ShopCart
{
    private $ajaxPost;

    private $arrayPost;

    private $sessionCart;

    protected static $availableMethods = [
        'cartAdd',
        'createdCart',
        'updateProduct',
        'deleteProductCart',
        'qtyUpdateProduct',
        'clearCart',
    ];

    /**
     * ShopCart constructor.
     * @throws ErrorException
     */
    function __construct()
    {
        session_start();

        if ($_POST) {
            $this->ajaxPost = $_POST;

            $this->arrayPost = json_decode($this->ajaxPost['json'], true);

            $this->sessionCart = ($_SESSION['cart'] ? $_SESSION['cart'] : array());

            $functionName = $this->ajaxPost['name'];

            if (!in_array($functionName, self::$availableMethods)) {
                throw new ErrorException('Unavailable cart method: ' . $functionName);
            }

            echo $this->$functionName($this->sessionCart, $this->arrayPost);
        }
        unset($_POST);
    }

    // Функция добавления товара в корзину

    public function cartAdd($cart, $array)
    {
        $counter = count($cart);

        if ($counter > 0) {
            $fixProduct = $this->findProduct($cart, $array);
        } else {
            $fixProduct = true;
        }

        if ($fixProduct === true && $array['qty'] > 0) {
            $counter = count($_SESSION['cart']);

            $_SESSION['cart'][$counter] = $array;
        }
    }

    // Функция вывода всех товаров в корзину

    public function createdCart($cart, $array)
    {
        if (count($cart) !== 0) {
            $i = 0;

            foreach ($cart as $arr) {
                $price = str_replace(['$', '.00', ' '], '', $arr['price']);

                $subtotal = $price * $arr['qty'];

                $substr = strlen($arr['img']) - 5;

                $cartImg = "/assets/img/basket/cart-" . substr($arr['img'], $substr);

                $result[$i] = array(
                    'name' => $arr['name'],
                    'img' => $cartImg,
                    'href' => $arr['href'],
                    'price' => "$" . $price . ".00",
                    'qty' => $arr['qty'],
                    'subtotal' => "$" . $subtotal . ".00"
                );

                $i++;
            }

            $result = json_encode($result);
        }

        return $result;
    }

    // Функция обновления колличества товаров в корзине на странице продукта

    public function updateProduct($cart, $array)
    {
        $counter = count($cart);
        if ($counter !== 0) {
            $qty = $this->findUpdateProduct($cart, $array);

            if (empty($qty)) {
                $qty = 0;
            }
        } else {
            $qty = 0;
        }

        return $qty;
    }

    // Функция удаления товара из корзину

    public function deleteProductCart($cart, $array)
    {
        $counter = count($cart);
        if ($counter !== 0) {
            $this->findDeleteProductCart($cart, $array);
        }
    }

    public function qtyUpdateProduct($cart, $array)
    {
        $counter = count($cart);
        if ($counter !== 0) {
            $output = $this->findQtyUpdateProduct($cart, $array);

            $result['subtotal'] = '$' . str_replace(['$', '.00', ' '], '', $output['price']) * $output['qty'] . '.00';

            $result = json_encode($result);
        }
        return $result;
    }

    // Функция удаления всей корзины

    public function clearCart()
    {
        unset($_SESSION['cart']);
    }

    // Вспомогательные функции

    private function findProduct($cart, $array)
    {
        foreach ($cart as $key => $val) {
            if ($val['href'] === $array['href']) {
                foreach ($val as $name => $value) {
                    if ($value !== $array[$name]) {
                        $_SESSION['cart'][$key][$name] = $array[$name];
                    }
                }

                if ($_SESSION['cart'][$key]['qty'] < 1) {
                    unset($_SESSION['cart'][$key]);
                    $_SESSION['cart'] = array_values($_SESSION['cart']);
                }

                return false;

                exit();
            };
        }
        return true;
    }

    private function findUpdateProduct($cart, $array)
    {
        foreach ($cart as $key => $arr) {
            if ($arr['href'] === $array['href']) {
                $qty = $cart[$key]['qty'];

                return $qty;

                exit();
            }
        }
        return false;
    }

    private function findDeleteProductCart($cart, $array)
    {
        foreach ($cart as $key => $arr) {
            if ($cart[$key]['href'] === $array['href']) {
                unset($_SESSION['cart'][$key]);

                $_SESSION['cart'] = array_values($_SESSION['cart']);
                return true;

                exit();
            }
        };
        return false;
    }

    private function findQtyUpdateProduct($cart, $array)
    {
        foreach ($cart as $key => $arr) {
            if ($cart[$key]['href'] === $array['href']) {
                if ($cart[$key]['qty'] !== $array['qty']) {
                    $_SESSION['cart'][$key]['qty'] = $array['qty'];
                };

                return $_SESSION['cart'][$key];

                exit();
            }
        };
        return false;
    }
}

return new ShopCart();
