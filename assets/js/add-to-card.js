var shopCart = (function () {
    
    return {
        CartPlusMinus: $('.cart-plus-minus'),
        ProductDetail: $('.product-details-content'),
        ProductPage: $(".shop-area"),
        BasketPage: $(".cart-main-area"),

        init : function(){
            
            if(this.ProductPage.length > 0){
                shopCart.updateProduct();
            }

            if(this.BasketPage.length > 0){
                shopCart.createdCart();
            }

            $("body").on("click", '.addtocart-btn', function () {
                shopCart.Resetlink();
                shopCart.addCart();
            });

            $('body').on("click", '.product-remove a', function () {
                shopCart.Resetlink();
                shopCart.deleteProductCart($(this));
            });
            
            $('body').on("click", '.cart-main-area .qtybutton', function(){
                shopCart.qtyUpdateProduct($(this));
            });

            $('body').on("click", '.cart-shiping-update-wrapper .cart-clear a', function(){
                shopCart.Resetlink();
                shopCart.clearCart();
            });

            $('body').on("click", '.cart-shiping-update-wrapper .cart-shiping-update button', function(){
                shopCart.Resetlink();
                shopCart.createdCart();
            });
        },

        addCart: function(){
            let info = {
                name : this.ProductDetail.find('h2').text() ,
                href : location.href,
                img : $('.product-details-img').find('img').attr('src'),
                price : this.ProductDetail.find('.product-price').find('.new').text(),
                qty : this.CartPlusMinus.find('input').val()
            }
            var json = JSON.stringify(info); 
            var name = 'cartAdd';
            shopCart.ajaxRequest(name,json); 
        },

        updateProduct: function(){
            let info = {
                href: location.href
            }
            var json = JSON.stringify(info); 
            var name = 'updateProduct';
            shopCart.ajaxRequest(name,json); 
            var input = this.CartPlusMinus.find('input');
            input.val('');
            setTimeout(function(){
                input.val(sessionStorage[name]);
            }, 50);
        },

        createdCart: function(){
            var form = this.BasketPage.find('.table-responsive').find('tbody');
            form.html('');

            var name = 'createdCart';
            shopCart.ajaxRequest(name, '');
            setTimeout(function(){
                if(sessionStorage[name]){
                    res = JSON.parse(sessionStorage[name]);
                    for (let i = 0; i < res.length; i++) { 
                        form.append(shopCart.cartRow(res[i]));
                    }   
                }else{
                    html = '<tr>\
                                <td style="text-align: center; width: 100%;">Ваша корзина пуста</td>\
                            </tr>';
                    form.append(html);
                }
            }, 30);   
        },

        cartRow: function(arr){
            html = '<tr>\
            <td class="product-thumbnail">\
                <a href="'+arr.href+'"><img src="'+arr.img+'" alt=\"\"></a>\
            </td>\
            <td class="product-name"><a href="'+arr.href+'">'+arr.name+'</a></td>\
            <td class="product-price-cart"><span class="amount">'+arr.price+'</span></td>\
            <td class="product-quantity">\
                <div class="cart-plus-minus">\
                    <div class="dec qtybutton">-</div>\
                    <input class="cart-plus-minus-box" type="text" name="qtybutton" value="'+arr.qty+'">\
                    <div class="inc qtybutton">+</div>\
                </div>\
            </td>\
            <td class="product-subtotal">'+arr.subtotal+'</td>\
            <td class="product-remove"><a href="#"><i class="ti-trash"></i></a></td>\
            </tr>';

            return html;
        },

        deleteProductCart: function(obj){
            let info = {
                href : obj.parent().parent().find('.product-name a').attr('href')
            }
            var json = JSON.stringify(info); 
            var name = 'deleteProductCart';
            shopCart.ajaxRequest(name,json);
            shopCart.createdCart();
        },

        qtyUpdateProduct: function(obj){
            let info = {
               qty: obj.parent().find('input').val(),
               href: obj.parent().parent().parent().find('.product-name a').attr('href')
            }
            var json = JSON.stringify(info); 
            var name = 'qtyUpdateProduct';
            shopCart.ajaxRequest(name,json);
            setTimeout(function(){
                if(sessionStorage[name]){
                    res = JSON.parse(sessionStorage[name]);
                    obj.parent().parent().parent().find('.product-subtotal').text(res.subtotal);
                }
            }, 30);   
        },

        clearCart: function(){
            name = 'clearCart';
            shopCart.ajaxRequest(name,'');
            shopCart.createdCart();
        },

        Resetlink: function(e) {
            var evt = e ? e : window.event;
            (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
            return false;
        },

        ajaxRequest: function(name,json){
            $.ajax({
                method: "POST", 
                url: "/plugins/shop_cart.php", 
                data: {
                name: name,
                json: json
                },
                success: function ( msg ) {
                    sessionStorage[name] = msg;
                }
            });    
        }
  };
})();

shopCart.init();

